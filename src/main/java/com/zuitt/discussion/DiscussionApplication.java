package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
public class DiscussionApplication {

	public static void main(String[] args) {

		SpringApplication.run(DiscussionApplication.class, args);
	}

	// Retrieve all posts
// localhost:8080/posts
	@GetMapping("/posts")
	@RequestMapping(value="/posts", method = RequestMethod.GET)
	public String getPosts(){
		return "All posts retrieved";

	}
	// Creating a new posts
// localhost:8080/posts
	//@RequestMapping(value = "/posts", method = RequestMethod.POST)
	@PostMapping("/posts")
	public String createPost() {
		return "New post created.";
	}

	// Retrieving a single post
// localhost:8080/posts/1234
// @RequestMapping(value = "/posts/{postid}", method = RequestMethod.GET)
	@GetMapping("/posts/{postid}")
	public String getPost(@PathVariable Long postid){
		return "Viewing details of post" + postid;
	}
	// Deleting a post
// localhost:8080/posts/1234
	@RequestMapping(value = "/posts/{postid}", method = RequestMethod.DELETE)
	public String deletePost(@PathVariable Long postid){
		return "The post " + postid + " has been deleted.";
	}

	// Updating a post
// localhost:8080/posts/1234
	@RequestMapping(value = "/posts/{postid}", method = RequestMethod.PUT)
// Automatically converts to format to JSON.
	@ResponseBody
	public Post updatePost(@PathVariable Long postid, @RequestBody Post post){
		return post;
	}

	// Retrieving a post for a particular user.
// localhost:8080/posts
// @RequestMapping(value = "/myPosts", method = RequestMethod.GET)
// @PutMapping("/myPosts")
// @DeleteMapping
	@GetMapping("/myPosts")
	public String getMyPosts(@RequestHeader(value = "Authorization") String user) {
		return "Posts for " + user + " have been retrieved";
	}


}
